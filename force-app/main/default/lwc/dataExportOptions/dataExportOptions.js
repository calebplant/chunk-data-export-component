import { api, LightningElement, wire } from 'lwc';
import getAvailableObjects from '@salesforce/apex/DataExportController.getAvailableObjects'

export default class DataExportOptions extends LightningElement {

    @api objectApiName;
    @api objectFields;

    @wire(getAvailableObjects)
    objectOptions;
    // objectOptions = ['Account', 'Account Thing', 'Contact', 'Bananas'];

    jobName;
    emailMe = false;
    recordsPerFile = 1000;
    selectedFields;

    get fieldButtonLabel() {
        return 'Update';
    }

    @api getUserOptions(){
        return {
            recordsPerFile: this.recordsPerFile,
            sendFinishEmail: this.emailMe,
            jobName: this.jobName
        }
    }

    // Handlers
    handeJobNameChange(event) {
        // console.log(event.target.value);
        this.jobName = event.target.value;
    }

    handleEmailMeChange(event) {
        console.log(event.target.checked);
        this.emailMe = event.target.checked;
    }

    handleRecordsPerPageChange(event) {
        // console.log(event.target.value);
        this.recordsPerFile = event.target.value;
    }

    handleSelectedFieldsChange(event) {
        // console.log(event.detail.value);
        this.selectedFields = event.detail.value;
    }

    handleConfirmFieldsUpdate(event) {
        console.log('DataExportOptions :: handleConfirmFieldsUpdate');
        console.log(JSON.parse(JSON.stringify(this.selectedFields)));
        if(!this.selectedFields.includes('Id')) {
            this.selectedFields.unshift('Id');
        }
        this.dispatchEvent(new CustomEvent('updatefields', {detail: this.selectedFields}));
    }

    handleLookupSearch(event) {
        console.log('DataExportOptions :: handleLookupSearch');
        const target = event.target;
        let searchTerm = event.detail.searchTerm;
                
        try {
            let searchResults = this.objectOptions.data.filter(eachOption => {
                return eachOption.label.toLowerCase().indexOf(searchTerm.toLowerCase()) >= 0;
            });
            console.log(searchResults);
            target.setSearchResults(searchResults);

        } catch(err) {
            console.log(err);
        }        
    }
    
    handleObjectLookupChange(event) {
        console.log('Selection changed!');
        const selectedName = event.detail;
        console.log(JSON.parse(JSON.stringify(selectedName)));

        this.dispatchEvent(new CustomEvent('updateobjname', {detail: event.detail[0]}));
    }
}