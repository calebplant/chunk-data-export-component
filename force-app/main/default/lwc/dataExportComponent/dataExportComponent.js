import { LightningElement, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getObjectFields from '@salesforce/apex/DataExportController.getReadableObjectFields';
import getPreviewRecords from '@salesforce/apex/DataExportController.getPreviewRecords';
import startExportJob from '@salesforce/apex/DataExportController.runExport';

export default class DataExportComponent extends NavigationMixin(LightningElement) {

    objectApiName;
    // objectFields;
    fieldData;
    selectedFields;
    previewRecords;
    previewColumns;
    // showExportDetails = true;
    jobId;
    taskId;
    orgUrl;

    @wire(getObjectFields, {objectApiName: '$objectApiName'})
    getFields(response) {
        if(response.data) {
            console.log('Successfully fetched object fields.');
            console.log(response.data);
            this.fieldData = response.data;
        }
        if(response.error) {
            console.log('Error fetching object fields!');
            console.log(response.error);
        }
    }

    @wire(getPreviewRecords, {objectApiName: '$objectApiName', fieldNames: '$selectedFields'})
    getPreviewRecords(response) {
        if(response.data) {
            console.log('Successfully fetched preview records.');
            console.log(response.data);
            this.previewRecords = response.data;
        }
        if(response.error) {
            console.log('Error fetching preview records!');
            console.log(response.error);
        }
    }

    get objectFields() {
        if(this.fieldData) {
            return this.fieldData.fields.map(eachField => {
                return { label: eachField.label, value: eachField.name}
            });
        }
        return null;
    }

    get showPreviewTable() {
        return this.previewColumns && this.previewRecords;
    }

    get isExportStarted() {
        return this.jobId && this.taskId;
    }

    get jobIdLabel() {
        return this.jobId.substring(0, this.jobId.length - 3);
    }

    // Handlers
    handleObjectNameChange(event) {
        console.log('DataExportComponent :: handleObjectNameChange');
        console.log(event.detail);
        if(event.detail) {
            this.objectApiName = event.detail;
        } else {
            this.objectApiName = undefined;
            this.fieldData = undefined;
        }
    }

    handleUpdateFields(event) {
        console.log('DataExportComponent :: handleUpdateFields');
        this.selectedFields = event.detail;
        this.previewColumns = this.selectedFields.map(eachSelectedFieldName => {
            return {
                label: this.fieldData.fieldLabelByName[eachSelectedFieldName],
                fieldName: eachSelectedFieldName,
                type: this.fieldData.fieldTypeByName[eachSelectedFieldName]
            }
        });
        console.log(JSON.parse(JSON.stringify(this.previewColumns)));
    }

    handleStartJob(event) {
        console.log('DataExportComponent :: handleStartJob');
        let payload = this.wrapPayload();
        console.log(payload);

        startExportJob({options: payload})
        .then(result => {
            console.log('Successfully started export.');
            console.log(result);
            this.jobId = result.jobId;
            this.taskId = result.taskId;
            this.orgUrl = result.orgUrl;
            window.scrollTo({left: 0, top: 0, behavior: 'smooth'});
        })
        .catch(err => {
            console.log('Error starting export!');
            console.log(err);
        })
    }

    handleClickMonitor(event) {
        console.log('DataExportComponent :: handleClickMonitor');
        let url = this.orgUrl + '/lightning/setup/AsyncApexJobs/home';
        window.open(url, '_blank');
    }

    handleClickViewTask(event) {
        console.log('DataExportComponent :: handleClickViewTask');
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.taskId,
                objectApiName: 'Task',
                actionName: 'view'
            }
        });
    }

    // Utils
    handleDebug() {
        console.log('HANDLE DEBUG');
        this.objectApiName = 'Account';
        console.log(this.objectApiName);
    }
    
    wrapPayload() {
        console.log('DataExportComponent :: wrapPayload');
        let exportOptions = this.template.querySelector("c-data-export-options").getUserOptions();
        return {
            objectApiName: this.objectApiName,
            fieldNames: this.selectedFields,
            chunkSize: exportOptions.recordsPerFile,
            jobName: exportOptions.jobName,
            sendFinishEmail: exportOptions.sendFinishEmail
        }
    }
}