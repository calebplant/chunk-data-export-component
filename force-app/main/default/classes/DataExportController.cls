public with sharing class DataExportController {

    private static Map<Schema.DisplayType, String> dataTableTypeByDisplayType = new Map<Schema.DisplayType, String>{
        Schema.DisplayType.BOOLEAN => 'boolean',
        Schema.DisplayType.CURRENCY => 'currency',
        Schema.DisplayType.DATE => 'date',
        Schema.DisplayType.DATETIME => 'date',
        Schema.DisplayType.DOUBLE => 'number',
        Schema.DisplayType.EMAIL => 'email',
        Schema.DisplayType.INTEGER => 'number',
        Schema.DisplayType.PERCENT => 'percent',
        Schema.DisplayType.PHONE => 'phone',
        Schema.DisplayType.URL => 'url'
    };
    
    @AuraEnabled(cacheable=true)
    public static FieldResponse getReadableObjectFields(String objectApiName)
    {
        System.debug('START getReadableObjectFields');
        FieldResponse response = new FieldResponse();

        // Get field map for object
        SObjectType recordType = ((SObject)(Type.forName('Schema.'+objectApiName).newInstance())).getSObjectType(); // See: https://salesforce.stackexchange.com/questions/218982/why-is-schema-describesobjectstypes-slower-than-schema-getglobaldescribe
        Map<String,Schema.SObjectField> fieldMap = recordType.getDescribe().fields.getMap();

        List<FieldData> fieldData = new List<FieldData>();
        Map<String, String> fieldTypeByName = new Map<String, String>();
        Map<String, String> fieldLabelByName = new Map<String, String>();
        for (Schema.SObjectField eachField : fieldMap.values())
        {
            Schema.DescribeFieldResult field = eachField.getDescribe();
            if(field.isAccessible()) {
                fieldData.add(new FieldData(field));
                String fieldType = dataTableTypeByDisplayType.get(field.getType()) != null ? dataTableTypeByDisplayType.get(field.getType()) : 'text';
                fieldTypeByName.put(field.getName(), fieldType);
                fieldLabelByName.put(field.getName(), field.getLabel());
            }
        }
        fieldData.sort();

        response.fields = fieldData;
        response.fieldTypeByName = fieldTypeByName;
        response.fieldLabelByName = fieldLabelByName;
        return response;
    }

    @AuraEnabled(cacheable=true)
    public static List<ObjectSearchResult> getAvailableObjects()
    {
        System.debug('START getAvailableObjects');
        List<ObjectSearchResult> result = new List<ObjectSearchResult>();
        List<EntityDefinition> entities = [SELECT QualifiedApiName, Label
                                            FROM EntityDefinition
                                            WHERE IsQueryable = True
                                            ORDER BY Label ASC];
        for(EntityDefinition eachEntity : entities) {
            result.add(new ObjectSearchResult(eachEntity));
        }
        System.debug(result);
        return result;
    }

    @AuraEnabled(cacheable=true)
    public static List<sObject> getPreviewRecords(String objectApiName, List<String> fieldNames)
    {
        System.debug('START getPreviewRecords');
        List<sObject> response = new List<sObject>();

        if(fieldNames.size() <= 0) {
            return null;
        }

        String queryFields = String.join(fieldNames, ', ');
        String query = 'SELECT ' + queryFields + ' FROM ' + String.escapeSingleQuotes(objectApiName) + ' LIMIT 5';
        System.debug(query);
        response = Database.query(query);
        return response;
    }

    @AuraEnabled
    public static Map<String, String> runExport(ExportOptions options)
    {
        System.debug('START runExport');
        System.debug(options);

        Task newTask = new Task();
        newTask.Subject = options.objectApiName + ' Export (' +  System.now() + ')';
        newTask.Description = 'Compilation of export files for ' + options.objectApiName + ' export.\nStarted: ' + System.now();
        newTask.Status = 'New';
        insert newTask;

        String queryFields = String.join(options.fieldNames, ', ');
        String query = 'SELECT ' + queryFields + ' FROM ' + String.escapeSingleQuotes(options.objectApiName);
        System.debug(query);

        DataExportBatchable batchable = new DataExportBatchable(options.objectApiName, query, options.fieldNames, newTask.Id);
        batchable.sendFinishEmail = options.sendFinishEmail;
        batchable.attachmentType = DataExportBatchable.ATTACHMENT_TYPE.FILE;
        batchable.jobName = options.jobName;

        Id jobId = Database.executeBatch(batchable, options.chunkSize);

        Map<String, Id> response = new Map<String, String>();
        response.put('jobId', jobId);
        response.put('taskId', newTask.Id);
        response.put('orgUrl', Url.getSalesforceBaseUrl().toExternalForm());
        return response;
    }

    public class ObjectSearchResult{
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String label {get; set;}
        
        public ObjectSearchResult(EntityDefinition def) {
            name = def.QualifiedApiName;
            label = def.Label;
        }
    }

    public class FieldResponse{
        @AuraEnabled
        public List<FieldData> fields;
        @AuraEnabled
        public Map<String, String> fieldTypeByName;
        @AuraEnabled
        public Map<String, String> fieldLabelByName;
    }

    public class FieldData implements Comparable{
        @AuraEnabled
        public String name;
        @AuraEnabled
        public String label;
        
        public FieldData(Schema.DescribeFieldResult field)
        {
            name = field.getName();
            label = field.getLabel();
        }

        public Integer CompareTo(Object ObjToCompare) {
            FieldData that = (FieldData)ObjToCompare;
            if (this.label > that.label) return 1;
            if (this.label < that.label) return -1;
            return 0;
        }
    }

    public class ExportOptions{
        @AuraEnabled
        public String objectApiName {get; set;}
        @AuraEnabled
        public List<String> fieldNames {get; set;}
        @AuraEnabled
        public Integer chunkSize {get; set;}
        @AuraEnabled
        public String jobName {get; set;}
        @AuraEnabled
        public Boolean sendFinishEmail {get; set;}
    }
}
