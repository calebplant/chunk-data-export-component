public with sharing class DataExportBatchable implements Database.Batchable<sObject>, Database.Stateful{

    public Enum ATTACHMENT_TYPE { ATTACHMENT, FILE }

    private final String objectLabel;
    private final String query;
    private final List<String> fields;
    private Id singleTaskId;
    private Integer batchCount=0;

    // Options
    public ATTACHMENT_TYPE attachmentType = ATTACHMENT_TYPE.FILE;
    public Boolean sendFinishEmail=false;
    public Boolean isMultiTask=false;
    public String jobName;

    // public DataExportBatchable(String passedLabel, String passedQuery, List<String> passedFields) {
    //     objectLabel = passedLabel;
    //     query = passedQuery;
    //     fields = passedFields;
    // }

    public DataExportBatchable(String passedLabel, String passedQuery, List<String> passedFields, Id taskId) {
        objectLabel = passedLabel;
        query = passedQuery;
        fields = passedFields;
        singleTaskId = taskId;
    }

    public Database.QueryLocator start(Database.BatchableContext BC){
        System.debug('START DataExportBatchable');
        // if(!isMultiTask && singleTaskId == null) {
            Task singleTask = generateSingleTask(BC);
            insert singleTask;
            System.debug('Inserted single Task');
            System.debug(singleTask);
            singleTaskId = singleTask.Id;
        // }
        return Database.getQueryLocator(query);
     }
   
     public void execute(Database.BatchableContext BC, List<sObject> scope){
        batchCount++;
        System.debug('EXECUTE DataExportBatchable');
        System.debug(singleTaskId);

        // CSV body
        String csvBody = getCsvBody(scope);

        Id targetTaskId;
        // if(isMultiTask) {
        //     // Create Task
        //     Task newTask = generateMultiTask(BC);
        //     insert newTask;
        //     System.debug('Inserted new Task');
        //     System.debug(newTask);
        //     targetTaskId = newTask.Id;
        // } else {
        //     targetTaskId = singleTaskId;
        // }
        targetTaskId = singleTaskId;

        switch on attachmentType {
            when FILE {
                insertRelatedContentDocument(BC, csvBody, targetTaskId);
            }
            when ATTACHMENT {
                insertRelatedAttachment(BC, csvBody, targetTaskId);
            }
            when else { // default to file
                insertRelatedContentDocument(BC, csvBody, targetTaskId);
            }
        }
      }
   
     public void finish(Database.BatchableContext BC)
     {
        System.debug('FINISH DataExportBatchable');
        
        if(sendFinishEmail) {
            System.debug('Sending finish email');

            OrgWideEmailAddress sender = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'calebplant@aggienetwork.com' LIMIT 1]; // Placeholder, obviously


            Messaging.SingleEmailMessage outboundEmail = new Messaging.SingleEmailMessage();
            outboundEmail.setTargetObjectId(UserInfo.getUserId());
            outboundEmail.setTreatTargetObjectAsRecipient(true);
            outboundEmail.setSaveAsActivity(false);
            outboundEmail.setUseSignature(false);
            outboundEmail.setSubject('Export Job Complete (' + BC.getJobId() + ')');
            outboundEmail.setHtmlBody(buildHtmlBody(BC));
            if(sender != null) {
                outboundEmail.setOrgWideEmailAddressId(sender.Id);
            }
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { outboundEmail });
        }
     }

     private void insertRelatedContentDocument(Database.BatchableContext context, String csvBody, Id newTaskId)
     {
        // Create CSV attachment
        ContentVersion csvConDoc = generateCsvContentDocument(context, csvBody, newTaskId);
        insert csvConDoc;
        csvConDoc = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :csvConDoc.Id LIMIT 1];  // Query to get ContentDocumentId
        System.debug('Inserted csvConDoc');
        System.debug(csvConDoc);
        // Link attachment to task
        ContentDocumentLink docLink = generateCsvDocLink(newTaskId, csvConDoc.ContentDocumentId);
        insert docLink;
        System.debug('Inserted DocLink');
        System.debug(docLink);
     }

     private void insertRelatedAttachment(Database.BatchableContext context, String csvBody, Id newTaskId)
     {
        Attachment newAttachment = generateCsvAttachment(context, csvBody, newTaskId);
        insert newAttachment;
        System.debug('Inserted newAttachment');
        System.debug(newAttachment);
     }

     private String getCsvBody(List<sObject> records)
     {
         // Header
         String result = String.join(fields, ',') + '\n';
        //  Rows
         for(sObject eachRecord : records) {
             List<String> recordValues = new List<String>();
             for(String eachField : fields) {
                recordValues.add(eachRecord.get(eachField) != null ? String.valueOf(eachRecord.get(eachField)) : '');
             }
             result += String.join(recordValues, ',') + '\n';
         }
         return result;
     }

     private Task generateSingleTask(Database.BatchableContext context)
     {
        System.debug('generating single task...');
        Task newTask = new Task();
        newTask.Subject = objectLabel + ' Export (' +  System.now() + ')';
        newTask.Description = 'Compilation of export files for ' + objectLabel + ' export.\nJob Id: ' + context.getJobId();
        newTask.Status = 'New';
        return newTask;
     }

    //  private Task generateMultiTask(Database.BatchableContext context)
    //  {
    //     System.debug('generating multitask...');
    //     Task newTask = new Task();
    //     newTask.Subject = objectLabel + ' Export #' + batchCount + ' (' + context.getJobId() + ')';
    //     newTask.Description = 'File # ' + batchCount + ' for ' + objectLabel + ' export.\nJob Id: ' + context.getJobId();
    //     newTask.Status = 'New';
    //     return newTask;
    //  }

     private ContentVersion generateCsvContentDocument(Database.BatchableContext context, String csvBody, Id taskId)
     {
        ContentVersion csvConDoc = new ContentVersion();
        csvConDoc.VersionData = Blob.valueOf(csvBody);
        System.debug('jobName: ' + jobName);
        String jobTitle = jobName != null ? jobName : context.getJobId();
        csvConDoc.Title = 'Export File ' + batchCount + ' for ' + objectLabel + ' (' + jobTitle + ')';
        csvConDoc.PathOnClient = objectLabel + '_' + batchCount + '_' + System.now().format('YYYY-MM-dd_HH-MM-ss') + '.csv';
        csvConDoc.ContentLocation = 'S';
        return csvConDoc;
     }

     private ContentDocumentLink generateCsvDocLink(Id taskId, Id attchId)
     {
        ContentDocumentLink docLink = new ContentDocumentLink();
        docLink.ContentDocumentId = attchId;
        docLink.LinkedEntityId = taskId;
        docLink.ShareType = 'I';
        docLink.Visibility = 'AllUsers';
        return docLink;
     }

     private Attachment generateCsvAttachment(Database.BatchableContext context, String csvBody, Id taskId)
     {
        Attachment csvAttch = new Attachment();
        csvAttch.Body = Blob.valueOf(csvBody);
        csvAttch.ContentType = 'text/csv';
        csvAttch.Description = 'File # ' + batchCount + ' for ' + objectLabel + ' export.\nJob Id: ' + context.getJobId();
        csvAttch.Name = objectLabel + '_' + batchCount + '_' + System.now().format('YYYY-MM-dd_HH-MM-ss') + '.csv';
        csvAttch.ParentId = taskId;
        return csvAttch;
     }

     private String buildHtmlBody(Database.BatchableContext context)
     {
        String taskUrl = Url.getSalesforceBaseUrl().toExternalForm() + '/' + singleTaskId;
        String body = 'Dear ' + UserInfo.getFirstname() + ',<br><br>' +
        'Your ' + objectLabel + ' export has completed. You can view the new task here: ' + taskUrl + '<br><br>' + 
        '<b>This is an automated, unmonitored email. If you have questions, please contact your administrator.</b>';
        return body;
     }
}
