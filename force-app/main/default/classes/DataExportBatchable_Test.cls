@isTest
public with sharing class DataExportBatchable_Test {
    @TestSetup
    static void makeData(){
        List<Account> accs = new List<Account>();
        for(Integer i=0; i < 10; i++) {
            accs.add(new Account(Name = 'testAcc' + i));
        }
        insert accs;
    }

    @isTest
    static void doesExecutableCreateTask()
    {
        Task newTask =  new Task(Subject='testTaskDesc', Description='testTaskDesc', Status='New');
        insert newTask;

        String query = 'SELECT Id, Name FROM Account';
        List<String> fieldNames = new List<String>{'Id', 'Name'};
        DataExportBatchable batchable = new DataExportBatchable('Account', query, fieldNames, newTask.Id);
        batchable.sendFinishEmail = True;
        batchable.attachmentType = DataExportBatchable.ATTACHMENT_TYPE.ATTACHMENT;
        batchable.jobName = 'testJobName';

        Test.startTest();
        Id jobId = Database.executeBatch(batchable, 100);
        Test.stopTest();

        List<Task> tasks = [SELECT Id FROM Task];
        System.assertNotEquals(0, tasks.size());
    }
}
