@isTest
public with sharing class DataExportController_Test {

    @TestSetup
    static void makeData(){
        List<Account> accs = new List<Account>();
        for(Integer i=0; i < 10; i++) {
            accs.add(new Account(Name = 'testAcc' + i));
        }
        insert accs;
    }

    @isTest
    static void doesGetAvailableObjectsReturnObjects()
    {
        Test.startTest();
        List<DataExportController.ObjectSearchResult> response = DataExportController.getAvailableObjects();
        Test.stopTest();

        List<String> objectLabels = new List<String>();
        for(DataExportController.ObjectSearchResult eachResult : response) {
            objectLabels.add(eachResult.label);
        }
        System.assert(objectLabels.contains('Account'));
        System.assert(objectLabels.contains('Contact'));
    }

    @isTest
    static void doesGetReadableObjectFieldsReturnObjectFields()
    {
        Test.startTest();
        DataExportController.FieldResponse response = DataExportController.getReadableObjectFields('Contact');
        Test.stopTest();

        System.assert(response.fieldLabelByName.keySet().contains('FirstName'));
        System.assert(response.fieldLabelByName.keySet().contains('LastName'));
    }

    @isTest
    static void doesGetPreviewRecordsReturnTruncatedRecordList()
    {
        String objectapiName = 'Account';
        List<String> fieldNames = new List<String>{'Name','Phone'};
        Test.startTest();
        List<sObject> objs = DataExportController.getPreviewRecords(objectApiName, fieldNames);
        Test.stopTest();

        System.assertEquals(5, objs.size());
        System.assert(((String)objs[0].get('Name')).startsWith('testAcc'));
    }

    @isTest
    static void doesRunExportEnqueueBatchable()
    {
        DataExportController.ExportOptions options = new DataExportController.ExportOptions();
        options.objectApiName = 'Account';
        options.fieldNames = new List<String>{'Name','Phone'};
        options.chunkSize = 100;
        options.sendFinishEmail = false;
        options.jobName = 'aJobName1';

        Test.startTest();
        Map<String, String> result = DataExportController.runExport(options);
        Integer numQueuedJobs = [SELECT COUNT() FROM AsyncApexJob];
        System.assertEquals(1, numQueuedJobs);
        Test.stopTest();

        List<Task> newTask = [SELECT Id FROM Task];
        System.assertNotEquals(0, newTask.size());
    }
}
