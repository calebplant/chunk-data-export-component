# Data Export Component

## Overview

A lightning web component allowing users to export records of any readable objects and their visible fields. Users may choose how many records are in each exported csv, and the exported files are attached to a new task upon export completion.

## Demo (~1.5 min)

[Here's a link to a demo video for the component](https://www.youtube.com/watch?v=bQZGk_PitCc)

## Some Screenshots

### Selecting an Object

![Selecting an Object](media/selecting-object.png)

### Previewing Output After Field Selection

![Previewing Output After Field Selection](media/record-preview.png)

### New Task

![New Task](media/new-task.png)

### Attached Files

![Attached Files](media/new-task-attachments.png)